Contributing to the openCFS userdocu
====================================

The mkdocs style is based on markdown language and super easy to edit.

**Please keep in mind, that this will be the documentation for users and not developers**

We're happy about contribuions to keep the documentation growing and up to date!
Below you find some details on how to do that.
If you have any questions, open an [issue](https://gitlab.com/openCFS/userdocu/-/issues).

To get statred editing the documentation (locally on your machine) the following steps are required

1. clone the [userdocu source repository](https://gitlab.com/openCFS/userdocu).
2. install [MkDocs](https://www.mkdocs.org/).
3. edit in the source, view the result locally.
4. commit your changes to a seperate branch, and push it to the gitlab server.
5. create a merge request on gitlab, assign it to a project maintainer for review.

Details can be found in the following.

## Installing MkDocs
It can be installed e.g. via `pip install mkdocs` or `conda install -c conda-forge mkdocs`
Attention: Use python 3.x, python 2.x will cause problems with the dependencies. The easiest way to get the installation up and running smoothly is to use conda as well as pip via conda (in the conda environment). You can do this by installing conda and then installing pip inside your conda environment via `conda install pip` 

**Extensions:**
* LaTex rendering: `pip install pymdown-extensions`
* Bibtex bibliographs: `pip install mkdocs-bibtex`

## Workflow
Simply clone the repo locally and execute `mkdocs serve`.
This generates a virtual Server, which can be visited with a browser (just like an ipython-notebook).
Just copy the http address http://127.0.0.1:8000 in your browser and the documentation is displayed.

Leave the browser open and start editing the markdown-files in the `/docs` folder.
By saving these files, the browser-page gets automatically updated...what you see is what you get principle (no compiling or building needed)

The basic structure of the docu is defined in the .yml file `mkdocs.yml` and can be live-edited as well

Everything must be done in the /docs folder, otherwise mkdocs cannot build it

# Structure for Tutorial-Examples
* Same structure as in the testsuite project, this means we have individual folders for the physical fields and the different examples/tutorials.
* One folder per example!
* Each example folder must contain a README.md file, which contains the documentation of the tutorial (referenced in the mkdocs.yml)
* Each example folder must contain the necessary files for the example (input mesh, xml file), so that it can be used in teaching as well!


# Collection of useful comments from collegues and students
* Erklärung der einzelnen RB's und Lasten (müssen halt PDE spezifisch gemacht werden)
* Erklärung zum Aufbau (Struktur) der xml Schema, z.B. muss in den PDEs die Region nochmal definiert werden, auf der gelöst werden soll 
* Beispiele mit Klick-Anleitung, wirklich ein einfaches Bsp von Null auf durchmachen Mesh->Simulation->Paraview 
* Angabe welche Materialparameter bei welcher Simulation gebraucht werden, z.B. Mechanik: E-Modul und Poisson-Zahl, Akustik: Dichte und Kompressionsmodul 
* Beschreibung der verschiedenen Geometriearten: 3D, axi, plane
* Ev. Links in welchen Bsp.en die jeweiligen tags des xml Schemas verwendet werden
