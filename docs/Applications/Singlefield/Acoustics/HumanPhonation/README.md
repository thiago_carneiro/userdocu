# Human Phonation

In this example, we will simulate the acoustic wave propagation inside a 3D model of the human vocal tract. This example is based on the simulation workflow that was used in the *simVoice* joint project of FAU Erlangen, TU Wien and TU Graz TODO_REFERENZEN

Thereby the acoustic sources for the used PCWE are computed from the incompressible pressure field that was obtained by a CFD simulation of the flow. Due to the size of the input data, the source files for this example are not provided in the documentation. Please contact [Andreas Wurzinger](https://online.tugraz.at/tug_online/pl/ui/$ctx/visitenkarte.show_vcard?pPersonenId=0E4E6DC7C9589C06&pPersonenGruppe=3) instead.

---

## Mesh

The acoustic Mesh was created with the commercial software *Ansys ICEM*. It consists 4 regions: The larynx region LARYNX, the vocal tract VT, the propagation region PR, and the perfectly matched layer region PML.

<!-- ![CAA Setup](caa_setup.pdf) -->

---

## XML flow pressure Interpolation

In order to perform a acoustic simulation on the presented mesh, the acoustic source data has to be interpolated onto it. Therefore, CFSdat (a subpackage included in openCFS) is used.

As openCFS does, CFSdat uses an [xml file](interpolatePressure.xml) to configure the workflow.

* Basic structure including XML header
```
<?xml version="1.0" encoding="UTF-8"?>
<cfsdat xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
	<pipeline>
	
		USERINPUT
	
	</pipeline>
</cfsdat>
```

* Define data length

First we define the length and timestep size of the CFD data
```
<stepValueDefinition>
	<startStop>
		<startStep value="0"/>              <!-- same as the first Ensight step number, but ATTENTION consider delta t !!! -->
		<numSteps value="12825"/>           <!-- step numbers to calculate -->
		<startTime value="1e-05"/>          <!-- start from the time that is equal to the 7501th step from the Ensight -->
		<delta value="1e-05"/>              <!-- delta t -->
		<deleteOffset value="no"/>          <!-- if NO then the time will be the same as in the CFD (Ensight) -->
	</startStop>
</stepValueDefinition>

```

* Define mesh input

The CFD data was exported in Ensight-Gold format as the file: 'CFD_results.case'.
```
<meshInput id="input" gridType="fullGrid">
	<inputFile>
		<ensight fileName="cfd_data_path/CFD_results.case">
			<variableList>
				<variable CFSVarName="fluidMechPressure" EnsightVarName="Pressure"/>
			</variableList>
		</ensight>
	</inputFile>
</meshInput>
```

* Conservative field interpolation

For the field interpolation the conservative cut-cell volume approach is used to interpolate the cell data onto the acoustic mesh saved in the hdf5 file 'CAA_mesh.h5'.
```
<interpolation type="FieldInterpolation_Conservative_CutCell" id="interp" inputFilterIds="input">
	<targetMesh>
		<hdf5 fileName="CAA_mesh.h5"/>
	</targetMesh>
	<singleResult>
		<inputQuantity resultName="fluidMechPressure"/>
		<outputQuantity resultName="fluidMechPressure_interpolated"/>
	</singleResult>
	<regions>
		<sourceRegions>
			<region name="Background"/>
		</sourceRegions>
		<targetRegions>
			<region name="LARYNX"/>
			<region name="VT"/>
		</targetRegions>
	</regions>
</interpolation>
```

* Define output

Finally we define the output filename by meshOutput id to save the interpolated pressure field in for both reagions LARYNX and VT.
```
<meshOutput id="InterpolatedPressureField" inputFilterIds="interp">
	<outputFile>
		<hdf5 extension="cfs" compressionLevel="6" externalFiles="no"/> <!-- compression level (greater number = bigger compression; default=1) -->
	</outputFile>
	<saveResults>
		<result resultName="fluidMechPressure_interpolated">
			<allRegions/>
		</result>
	</saveResults>
</meshOutput>
```

* Run CFSdat

```shell
cfsdat interpolatePressure
```

---

## XML Source term computation

As we now have obtained the conservatively interpolated incompressible flow pressure on the acoustic grid, we again use CFSdat to compute the RHS source of the PCWE $\partial p / \partial t$. The convective part of the RHS source is neglected in this example due to its negligible impact on the resulting sound spectrum in the far field (see references).

The [xml file](calc_dpdt.xml) is structured as before with the inputs:

* Define data length
```
<stepValueDefinition>
	<startStop>
		<startStep value="0"/>              <!-- same as the first Ensight step number -->
		<numSteps value="12825"/>           <!-- step numbers to calculate -->
		<startTime value="1e-05"/>          <!-- start from the time that is equal to the 7501th step from the Ensight -->
		<delta value="1e-05"/>              <!-- delta t -->
		<deleteOffset value="no"/>          <!-- if NO then the time will be the same as in the CFD (Ensight) -->
	</startStop>
</stepValueDefinition>
```

* Define input

This time we define the previously computed interpolated flow pressure data.
```
<meshInput id="input" gridType="fullGrid">
	<inputFile>
		<hdf5 fileName="results_hdf5/InterpolatedPressureField.cfs"/>
	</inputFile>
</meshInput>
```

* Define time derivative
```
<timeDeriv1 inputFilterIds="input" id="pressure_time_derivative">
	<singleResult>
		<inputQuantity resultName="fluidMechPressure_interpolated"/>
		<outputQuantity resultName="acouRhsLoad"/>
	</singleResult>
</timeDeriv1>
```

* Define output
```				  
<meshOutput id="source_dpdt" inputFilterIds="pressure_time_derivative">
	<outputFile>
		<hdf5 extension="cfs" compressionLevel="6" externalFiles="no"/><!-- compression level (greater number = bigger compression; default=1) -->
	</outputFile>
	<saveResults>
		<result resultName="acouRhsLoad">
			<allRegions/>
		</result>
	</saveResults>
</meshOutput>
```

---

## XML Simulation Setup

For the propagation simulation 

* Basic structure including XML header

```
<?xml version="1.0"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.cfs++.org/simulation">
    
    FILE_FORMATS
    
	DOMAIN
    
    <sequenceStep>
        
		ANALYSIS_TYPE
        
        <pdeList>

			PDE
            
        </pdeList>
        
        SOLVER_SETTINGS
        
        
    </sequenceStep>
```

* File formats

```
<fileFormats>
	<input>
		<hdf5 fileName="source_dpdt.cfs"/>
	</input>
	<output>
		<hdf5 id="h5"/>
		<text id="txt"/>
	</output>
	<materialData file="mat.xml" format="xml"/>
</fileFormats>
```

* Computational domain

```
<domain geometryType="3d">
	<regionList>
		<region name="LARYNX" material="air"/>
		<region name="VT" material="air"/>
		<region name="PR" material="air"/>
		<region name="PML" material="air"/>
	</regionList>
	<ncInterfaceList>
		<ncInterface name="IF1" masterSide="IF_VT" slaveSide="IF_PR"/>
	</ncInterfaceList>
	<nodeList>
		<nodes name="mic">
			<coord x="0.24657" y="0.009" z="-0.049069"/>
		</nodes>
	</nodeList>
</domain>
```

* Analysis Type

```
<analysis>
	<transient>
		<numSteps>12825</numSteps> 
		<deltaT>1e-5</deltaT>
	</transient>
</analysis>
```

* Acoustic PDE

```
<acoustic formulation="acouPotential" timeStepAlpha="-0.3">
    <regionList>
        <region name="LARYNX"/>
        <region name="VT"/>
        <region name="PR"/>
        <region name="PML" dampingId="dampPML2"/>
    </regionList>
    <ncInterfaceList>
        <ncInterface name="IF1" formulation="Nitsche" nitscheFactor="50"/>
    </ncInterfaceList>
    <dampingList>
        <pml id="dampPML2">
            <propRegion>
                <direction comp="x"     min="0.19"      max="0.275"/>
                <direction comp="y"     min="-0.051"    max="0.069"/>
                <direction comp="z"     min="-0.0525"   max="0.0675"/>
            </propRegion>
            <type>inverseDist</type>
            <dampFactor>1.0</dampFactor>
        </pml>
    </dampingList>
    <bcsAndLoads>
        <absorbingBCs volumeRegion="LARYNX" name="IF_ABC"/>
        <rhsValues name="LARYNX">
            <grid>
                <defaultGrid quantity="acouRhsLoad" dependtype="GENERAL" >
                    <globalFactor> ((t lt 8e-4)? (-1.0)/(1.204*343.4*343.4)*(1*(1-(cos(0.5*pi/8e-4*t))^2)) : (-1.0)/(1.204*343.4*343.4))</globalFactor>
                </defaultGrid>
            </grid>
        </rhsValues>
        <rhsValues name="VT">
            <grid>
                <defaultGrid quantity="acouRhsLoad" dependtype="GENERAL">
                    <globalFactor> ((t lt 8e-4)? (-1.0)/(1.204*343.4*343.4)*(1*(1-(cos(0.5*pi/8e-4*t))^2)) : (-1.0)/(1.204*343.4*343.4))</globalFactor>
                </defaultGrid>
            </grid>
        </rhsValues>
    </bcsAndLoads>
    <storeResults>
        <nodeResult type="acouPotentialD1">
            <allRegions/>                   
            <nodeList>
                <nodes name="mic" outputIds="txt"/>
            </nodeList>           
        </nodeResult>                
    </storeResults>
</acoustic>
```

* Solver Settings

```
<linearSystems>
	<system>
		<solverList>
			<pardiso id="default">
			</pardiso>
		</solverList>
	</system>
</linearSystems>
```

* Run the simulation

```shell
cfs propagation
```

---

## Results

<!-- Thereby the wave is excited by a point source on the right side of the barrier in the origin $[x,y,z]=(0,0,0)$ of the domain.

For the analysis type, we chose a harmonic analysis in the frequency range of 34Hz to 680Hz with 10 linearly spaced samples.

Since we cannot create an infinitely large computational domain, we have to impose a model of free-radiation, in this example, we are using the PML (Perfectly Matched Layer) technique, described [here](../../../../Tutorials/Features/pml.md).

The obtained acoustic pressure field can then be visualized in Paraview

![Acoustic Pressure](acouPressure.png)

And with the data at the defined microphone position we can calculate the spectrum of the sound pressure level.

![Spectrum](SPL.pdf) -->

---
