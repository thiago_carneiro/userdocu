# Analysis Workflow

This tutorial illustrates the typical analysis workflow on the example of heat conduction in 2D.

## Problem Definition


We consider stationary heat conduction through a wall in 2D.
Temperatures are given for the left and right boundary.
No heat flux through the top and bottom boundary.

What is the heat flux through the wall?


```
Sketch of the domain

  ___________
 |           |    ^
 |           |    |
 |   wall    |    b
 |           |    |
 |___________|    v

 <---- a ---->
  
```

![Sketch of the domain](Sketch-heat.png)


## Boundary Conditions

Boundary conditions used for heat conduction problems are the following:

* Temperature specified on the boundary (Dierichlet): $`T(x,t) = T_{e}(x,t)`$
   
* Heat flux across the boundary (Neumann): $`q.n = q_n`$ or $`q.n = 0`$
   
* Heat transfer to another medium with reference temperatude $`T_r`$ and heat transfer coefficient $`\alpha`$ on the boundary: $`q.n = \alpha *(T - T_r)`$
    
* Thermal radiation to free space on the boundary: $`q.n = q_tr = \rho *T^4`$


* For the actual example, Dierichlet B.C. have been set.


## Meshing


We use the *Trelis* GUI to create the domain:

1. Start it by typing `trelis` on the terminal.
2. Create a rectange
3. Mesh it
4. Assign and rename a *Block* for the *wall* domain
5. Use *Nodeset* for the boundaries
6. Export the mesh as a ANSYS-cdb file

The file [`wall.cdb`](wall.cdb) was created this way.

## Simulation with CFS

Use an XML-editor (e.g. *oXygen* or *eclipse*) to define the simulation input for CFS.

The input file [`simulation-input.xml`](simulation-input.xml) is the simulation input.
In the file [`mat.xml`](mat.xml) the material porperties are defined.
Both files are complete, and can be used for the example problem.

To start the computation run the following command in the terminal
```shell
cfs -p simulation-input.xml job
```
where `job` can be any name you choose for the simulation.

CFS will write some output on the terminal, and produce the two files
* `job.info.xml`, which contains some details about the run, and
* `job.cfs` in the `results_hdf5` directory, which you can view with ParaView.


## Postprocessing with ParaView


Open ParaView (by typing `paraview` in a terminal) and load the result file using File, Open... then click Apply.
