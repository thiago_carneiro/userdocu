Piezoelectric Unit Cube
=======================

Problem Definition
--------------------

This example illustrates the linear piezoelectric material, by considering a simple unit cube made from piezoelectric material.
The faces of the cube are called like the compass, N (north), E (east), S (south), W (west) for the +y, +x, -y, -x normal faces, and T (top) and B (bottom) for the +z and -z normal faces, respectively.
The lines are named by the adjoining faces, e.g. `L_NW` for the line created by the intersection of north-surface `S_N` and `S_W` surface-west.
The same is done for corner points, e.g. `P_NEB`.

We set statically determined displacement BCs on three nodes, and apply an electric field in negative z-direction.

```
Sketch of the domain

+------+.
|`.    | `.
|  `+--+---+
|   |  |   |
+---+--+   |
 `. |   `. |
   `+------+


```

![Sketch of the domain](Sketch-piezo.png)


Meshing
------------

1. Type 'trelis' on the terminal. 
2. On the *Trelis* GUI open up the journal file [`UnitCube.jou`](UnitCube.jou) and run it.
3. A geometry of an unit cube is created.
4. Export the geometry as an ANSYS-cdb mesh file `UnitCube.cdb`
5. You can also save the created geometry as `UnitCube.trelis` to open it directly on *Trelis* (optional).

The file [`UnitCube.cdb`](UnitCube.cdb) was created this way.


Simulation with CFS
---------------

Use an XML-editor (e.g. *oXygen* or *eclipse*) to define the simulation input for CFS.

For all the cases, we have the following generic procedure: 

The input file [`piezo_static.xml`](piezo_static.xml) is the simulation input.
In the file [`piezo_mat.xml`](piezo_mat.xml) the material porperties are defined.

To start the computation run the following command in the terminal

```shell
cfs -p piezo_static.xml job
```
or run the simulation by executing [`./run.sh`](run.sh).

where `job` can be any name you choose for the simulation.

CFS will write some output on the terminal, and produce the two files
* `job.info.xml`, which contains some details about the run, and
* `job.cfs` in the `results_hdf5` directory, which you can view with ParaView.


Postprocessing
--------------

Open ParaView and choose *File -> Load State ...* to load the provided visualization pipeline [`post.pvsm`](post.pvsm)

Further Suggestions
--------------------

Is the deformation expected?
You could also compute the results analytically from the linearized piezoelectric material law.

Additionally, you can modify the example to answer the following questions:

* Switch to fully constraint BCs and compute the stress state. Is the result expected?
* What happens when you change the orientation of the polarization?
