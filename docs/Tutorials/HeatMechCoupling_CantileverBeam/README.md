Thermal-Mechanic-Coupling
=========================

This is a simple expampe of a beam bending under terperature load.
It illustrates the forward coupling between a heat conduction analysis (computing the temperature field) and a subsequent mechanic analysis computing the displacement field due to temperature loading.
The first sequence step is used to create a linear temperature distribution over the thickness of the beam.
In the second step this is applied as an input for thermal strain.
Due to the chosen mechanical boundary conditions there are considerable thermal stresses.

Exercise Suggestions
--------------------

To get familiar with the example

* study the trelis journal file [`beam2D.jou`](beam2D.jou) to find out which regions are defined
* look into the simulation input [`heat-mech.xml`](heat-mech.xml) and got throug the analysis steps
* run everything (e.g. using [`run.sh`](run.sh)) and open the output in ParaView
* load the paraview state file [`sigma-xx.pvsm`](sigma-xx.pvsm)

Additionally you can modify the example to

* compute the von-Mises equivalent stress
* choose boundary conditions at the left edge which do not over-constrain the vertical displacement. How do the results change?
