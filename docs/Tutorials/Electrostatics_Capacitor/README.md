Capacitor in 3D
===============

Problem Definition
--------------------
A rotational symmetric capacitor is modelled in 3D as a quarter model.
The rotationaly symmetry is not used to illustrate 3D modelling in Trelis.

The capacitor plates have a radius r  and are seperated by d.
We use a potential difference of V between the electrodes.

The capacitance can be computed from the total energy U = 1/2*C*V^2.


```

Sketch of the domain

                        <-V->
                        <-d->

;;iiiiiiiiiiiiiiiiiiiii;;;;;;;iiiiiiiiiiiiiiiiiiiiiii;;    ^
;;tCCCCCCCCCCCCCCCCCCCG1;;;;;i                      t;;    |  
i;1GCCCCCCCCCCCCCCCCCCG1;;;;;i                      1;i    |  r
 ;1GCCCCCCCCCCCCCCCCCCG1;;;;;i                      1;     v
   LCCCCCCCCCCCCCCCCG1iiiiiiii1111111111111111111111       
    L////////////////////////////////////////////L1
      L//////////////////////////////////////////L
        L////////////  air domain  ////////////L 
          L//////////////////////////////////L
              L/////////////////////////L
                    LLLLLLLLLLLLLLLL
                      
```

![Sketch of the domain](Sketch-capacitor.png)


Meshing
------------

1. Type 'trelis' on the terminal. 
2. On the *Trelis* GUI open up the journal file [`capacitor.jou`](capacitor.jou) and run it.
3. A geometry of a rotational symmetric capacitor modelled in 3D as a quarter model is created.
4. Export the geometry as an ANSYS-cdb mesh file [`capacitor.cdb`](capacitor.cdb).
5. You can also save the created geometry as `capacitor.trelis` to open it directly on *Trelis* (optional).
6. Look into the journal file [`capacitor.jou`](capacitor.jou) to see the Trelis commands to create the mesh.
7. These commands could also have been run interactively in Trelis.

The file [`capacitor.cdb`](capacitor.cdb) was created this way.

Simulation with CFS
---------------

Use an XML-editor (e.g. *oXygen* or *eclipse*) to define the simulation input for CFS.

The input file [`capacitor.xml`](capacitor.xml) is the simulation input.
In the file [`mat.xml`](mat.xml) the material porperties are defined.
Both files are complete, and can be used for the example problem.

To start the computation run the following command in the terminal
```shell
cfs -p capacitor.xml job
```
where `job` can be any name you choose for the simulation.

CFS will write some output on the terminal, and produce the two files
* `job.info.xml`, which contains some details about the run, and
* `job.cfs` in the `results_hdf5` directory, which you can view with ParaView.


Postprocessing
--------------

1. use ParaView to visualize the field results in `results_hdf5/capacitor.cfs`. You can use the state file [`post.pvsm`](post.pvsm)
2. study the integrated results in `history/*`, e.g. the electric energy in the region between the capcitor plates saved in `capacitor-elecEnergy-region-V_elec.hist`
3. campare with the analytic estimates


Further Suggestions
-------------------------

You can modify the example to answer the following questions:

* How large is the influence of the stray field on the total capacitance?
* How large must the air domain around the capacitor be modelled?
* What changes if one uses polyimide between the electrodes?
