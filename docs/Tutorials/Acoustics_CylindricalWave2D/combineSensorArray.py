#!/usr/bin/env python

def collectSensorArray(filename) :
  """
  collects data from all files called 'filename-*' into one file

  The output file is called 'filename.hist'. It contains the sensor
  data in the columns and one row per time/frequency set, i.e. original file.
  """
  from glob import glob
  from numpy import array,loadtxt,savetxt,zeros,argsort,arange

  files = array(glob('%s-*'%filename))
  if len(files) == 0:
    print("No files found for input '%s'"%filename)
    raise SystemExit
  # sort files correctly
  It = array([int(f.split('-')[-1]) for f in files]) # time index is last part in filename-n
  Is = argsort(It)
  files = files[Is]

  # load first file and get element numbers
  data = loadtxt(files[0])
  # 6 columns for 2D (data is in 6/2 = 3rd column)
  def getI(ncol) :
    if ncol%2 == 0: #even -> real
      return array([int(ncol/2)]) 
    else : # odd -> complex
      return array([0,1])+int(ncol/2)

  #origElemNum,globCoord-x,globCoord-y,acouPressure-,locCoord-xi,locCoord-eta
  if len(data.shape) == 1: # only one point
    ncol = len(data)
    eids = [int(data[0])]
    I = getI(ncol)
    getdata = lambda x,i: array([x[i]])
  else :
    ncol = data.shape[1]
    eids = array(data[:,0],dtype=int)
    I = getI(ncol)
    getdata = lambda x,i: x[:,i]
  # read coordinated from info
  globCoord = getdata(data,arange(1,I[0])) # global 
  locCoord = getdata(data,arange(I[-1]+1,ncol)) # local
  #print(globCoord,globCoord.shape)

  # write a header
  headerlines = []
  headerlines.append('File created from files %s-*'%filename)
  headerlines.append('origElemNum: '+' '.join(['%i'%val for val in eids]))
  cs='xyz'
  for i in range(I[0]-1):
    headerlines.append('globCoord-%s: '%(cs[i])+' '.join(['%.18e'%val for val in globCoord[:,i]]))
  for i in range(I[0]-1):
    headerlines.append('locCoord-%s: '%(cs[i])+' '.join(['%.18e'%val for val in locCoord[:,i]]))

  # set precision
  prec=12
  
  # extract values from single files
  if len(I)==2 : #complex
    vals = zeros([len(files),len(eids)],dtype=complex)
    for i,f in enumerate(files) :
      data = loadtxt(f,usecols=I)
      vals[i,:] = getdata(data,0)+1j*getdata(data,1)
    savetxt('%s.hist'%filename,vals,header='\n'.join(headerlines),fmt=' '.join(['%%.%ie%%+.%iej'%(prec,prec)]*len(eids)))
  else :
    vals = zeros([len(files),len(eids)])
    for i,f in enumerate(files) :
      vals[i,:] = loadtxt(f,usecols=I)
    savetxt('%s.hist'%filename,vals,header='\n'.join(headerlines),fmt=' '.join(['%%.%ie'%(prec)]*len(eids)))

if __name__ == "__main__" :
  import argparse
  import sys
  parser = argparse.ArgumentParser(description='collect files from sensorArray output into one single file')
  parser.add_argument('filename', type=str, default=sys.stdin)
  args = parser.parse_args()

  #print(args.filename)
  collectSensorArray(args.filename)
