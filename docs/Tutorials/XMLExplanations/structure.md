# XML Input Scheme Explanations
This is just a summary of the most important and common xml commands and structure, for a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse


## Skeleton of an OpenCFS xml input file
The underlying structure of our xml files is defined via a so-called xml-scheme.
The path to this scheme is defined in the header of every xml file and per default it points to our default public scheme, provided on our gitlab server:
```
<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
```

In general, an OpenCFS xml file consists of three main parts:

* fileFormats: definition of input-/output- and material-files
* domain: region definitions (assign material to different regions)
* sequenceStep: contains the definition of the analysis type and the PDE. Several sequenceSteps can be concatenated and results from previous sequenceSteps can be processed in order to produce the desired result.


```
<!-- define which files are needed for simulation input & output-->
<fileFormats>
    <input>
        <!-- read mesh -->
    </input>
    <output>
        <!-- define output -->
    </output>
    <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>

<domain geometryType="3d">
    <regionList>
        <!-- region definition -->
    </regionList>
</domain>

<sequenceStep index="1">
    <analysis>
        <!-- analysis type: static, transient, harmonic, multiharmonic, eigenfrequency -->
    </analysis>
    <pdeList>
        <!--for example consider the electric conduction PDE-->
        <elecConduction>
            <regionList>
                <!--define on which regions the PDE is solved-->
            </regionList>
            <bcsAndLoads>
                <!--define appropriate BC's-->
            </bcsAndLoads>
            <storeResults>
                <!--define the results to be stored-->
            </storeResults>
        </elecConduction>
    </pdeList>
</sequenceStep>
</cfsSimulation>
```


## Detailed Description
In the following the most important, PDE-independent, tags are described.
For a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse


### `fileFormats` Section

* `<input>` (mandatory) defines which files are read as input (either data and/or mesh)
* Different inputs are possible `gmsh`, `cdb`, `ensight`, `hdf5`. Several more are available in the xml scheme but they are not maintained
* When reading more than one file, unique `id's` have to be provided
* `scaleFac=` (optional) scales the provided mesh with a prescribed factor, e.g. conversion from mm to m
* `<scatteredData>` (optional) can read some pointwise data, for example from a csv file
* `<output>` (mandatory) defines the output format, most of the time you want to export the results in h5 format, sometimes also in text format, e.g. global integrated quantities
* `<materialData>` (mandatory) defines the location of the material file

```
<fileFormats>
    <input>
      <gmsh fileName="../meshes/model_indwat.msh2" id="i1" scaleFac="1e-3"/>
      <gmsh fileName="../meshes/model_air.msh2" id="i2"/>
      <gmsh fileName="../meshes/model_sheet.msh2" id="i4"/>
    </input>
    <scatteredData> 
        <csv fileName="testinput.csv" id="inCSV">
            <coordinates>
                <comp dof="x" col="1"/>
                <comp dof="y" col="2"/>
                <comp dof="z" col="3"/>
            </coordinates>
            <quantity name="heatSourceDensity" id="hsd1">
                <comp dof="x" col="0"/>
            </quantity>
        </csv>
    </scatteredData>
   <output>
      <hdf5/>
      <text id="txt"/>
   </output>
   <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>
```


### `domain` Section

* `<regionList>` (mandatory) define the material of every region in the mesh file(s)

```
  <domain geometryType="plane">
    <regionList>
      <region material="air" name="region_1"/>
      <region material="iron" name="region_2"/>
      <region material="air" name="region_3"/>
    </regionList>
  </domain>
```


