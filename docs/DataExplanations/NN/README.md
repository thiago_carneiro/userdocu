# Nearest Neighbour

The nearest neighbor interpolation of openCFS rests upon the inverse distance weighting (Shepard's method). 

![explanation](explanation.png)

Based on the defined number of neighbours $n$ from the source mesh, the nearest neighbours are searched and their distance $r$ to the considered node is computed. 
Based on this distances the weights $w$ are computed.
\begin{equation}
w_i = \left( \frac{R_{\mathrm{ max}}-r_i}{R_{\mathrm{ max}} r_i} \right)^p \, ,
\end{equation}
with $R_\mathrm{ max} = 1.01 r_{\mathrm{ max}}$ as being 1.01 times of the maximal distance $r_{\mathrm{ max}}$, and $p$ as the interpolation exponent. Shepard stated $1 \leq p \leq 3$. Increasing $ p $ means that values that are further away are taken into account more. Finally, each value of each node $v_1$ that is taken into account is weighted to compute the new value $v_{\mathrm n}$ of the considered node
\begin{equation}
v_{\mathrm n} = \sum_{i=1}^{n} \frac{w_i v_i }{\sum_{i=1}^{n}w_i} \, .
\end{equation}


In CFSDat the Nearest Neighbor interpolation is defined in the xml-file as following:
```
<interpolation type="FieldInterpolation_NearestNeighbour" inputFilterIds="..." id="...">
    <scheme interpolationExponent="..." numNeighbours="..." globalFactor="..."/>                   
    <targetMesh>
        <hdf5 fileName="..."/>
    </targetMesh>
    <singleResult>
        <inputQuantity resultName="..."/>
        <outputQuantity resultName="..."/>
    </singleResult>
    <regions>
        <sourceRegions>
            <region name="..."/>
        </sourceRegions>
        <targetRegions>
            <region name="..."/>
        </targetRegions>
    </regions>
</interpolation>
```

*inputFilterIds* must refer to the id of an input filter which contains the defined "sourceRegion" and "quantity". As target mesh, any compatible file input can be used (see chapter Data Input/Output).



Apply Shepard interpolation cf. Numerical Recipes 3rd ed. p. 143ff.
or http://www.ems-i.com/gmshelp/Interpolation/Interpolation_Schemes \
 /Inverse_Distance_Weighted/Shepards_Method.htm
 
 
interpolationExponent = Exponent for calculation of interpolation weight function.

globalFactor = Global Factor for scaling the result (usually 1)

numNeighbours = Number of considered Nodes
