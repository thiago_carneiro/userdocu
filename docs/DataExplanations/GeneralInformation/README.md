# General Information

Establishing an xml file for CFSdat it is fundamental that the pipeline, existing of different CFSdat filters, is closed. The pipeline hast to start with the step value definition and has to be followed by the input filter and end with the output filter. In between multiple filters can be added, serially or parallelly.

## Defining Step Value Definition
It is possible to define input data for the time and frequency domain. However, not all filters are capable of processing data in the frequency domain. 
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>

  </pipeline>    
```    

## Defining Filters
 
| Single or serial Filters: | Multiple, parallel Filters: |
| ----------- | ----------- |
| ![pipeline](pipeline.png) | ![MultiPipeline](MultiPipeline.png) |



</br>
** Which can be defined in the xml as: ** </br>
Single or serial Filters:
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>
    
    <meshInput id="inputFilter" gridType="fullGrid"  >      

    </meshInput>
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp1" inputFilterIds="inputFilter">
    </interpolation>       
    
    <meshOutput id="Outout" inputFilterIds="interp1">

    </meshOutput>
  </pipeline>
```
</br>
Multiply, parallel Filters:
```
  <pipeline>
  
     <stepValueDefinition>
      <startStop>
        <startStep value="..."/>
        <numSteps value="..."/> 
        <startTime value="..."/>
        <delta  value="..."/>
        <deleteOffset  value="no"/>
      </startStop>
    </stepValueDefinition>
    
    <meshInput id="inputFilter" gridType="fullGrid"  >      

    </meshInput>

    <interpolation type="FieldInterpolation_Cell2Node" id="interp1" inputFilterIds="inputFilter">
    </interpolation> 
       
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp2" inputFilterIds="inputFilter">
    </interpolation>    
    
    
    <interpolation type="FieldInterpolation_Cell2Node" id="interp3" inputFilterIds="inputFilter">
    </interpolation>       
    
    <meshOutput id="Outout" inputFilterIds="interp1 interp2 interp3">

    </meshOutput>
  </pipeline>
```





** Link all CFSdat Filter to Testsuite examples **

Give overview with short explanation of all possible Filter



## Available Filters

### Interpolation Filters

Each interpolation filter, that is not conservatively, can be performed in the time and frequency domain.  

* ** Cell2Node/Node2Cell **</br>
Allows to interpolate nodal based data onto cells and vice versa. 


* **Nearest Neighbour**
* **FE Based**
* **Radial Basis Functions**

### Conservative Interpolation Filters
* ** CellCentroid **  </br>
Allows conservatively interpolating scalar quantities.
* ** CutCell ** </br>
Allows conservaively interpolating scalar quantities, using an advanced cut cell algorithm. Requires more computational ressources than CellCentroid.  

### Aeroacoustic Source Terms
* ** LambVector ** </br>
AeroacousticSource_LambVector
* ** LighthillSourceTerm ** </br> 
AeroacousticSource_LighthillSourceTerm
* ** LighthillSourceVector ** </br> 
AeroacousticSource_LighthillSourceVector
* ** TimeDerivative ** </br>
deriv

### Synthetic Sources
* **SNGR** </br>

### Data Processing
* **Curl** </br>
SpaceDifferentiation_Curl
* **Divergence** </br> 
SpaceDifferentiation_Divergence
* **Gradient** </br>
SpaceDifferentiation_Gradient
* **TimeMean** <br>
timeMean
* **FIR**
* **Temporal Blending**

### Utils
* **VolumeMultiplication**

 

# References
\bibliography
