# FAQ

## General Information 

* CFSdat can not read data from curved surfaces. 

## Reading Ensight Data 

* The ensight reader can only process time steps with not more than 6 digits.
* The ensight reader can not process vertex located data (only element based). 



